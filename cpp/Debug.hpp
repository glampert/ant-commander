
// ================================================================================================
// -*- C++ -*-
// File: Debug.hpp
// Author: Guilherme R. Lampert
// Created on: 16/09/13
// Brief: Debug logger.
// ================================================================================================

#ifndef DEBUG_HPP
#define DEBUG_HPP

#include <fstream>

class DebugLog
{
public:

	static DebugLog & Instance();
	void Open(const std::string &filename);
	void Close();
	~DebugLog();

	friend DebugLog & operator << (DebugLog & bug, std::ostream & (*manipulator)(std::ostream &))
	{
		bug.file << manipulator;
		bug.file.flush();
		return bug;
	}

	template<class T> friend DebugLog & operator << (DebugLog & bug, const T &t)
	{
		bug.file << t;
		bug.file.flush();
		return bug;
	}

private:

	DebugLog();
	std::ofstream file;
};

#if defined (ANTS_DEBUG)

	// Log a message in the debug file:
	#define LOG(x) DebugLog::Instance() << x << std::endl

	// Debug assert:
	#define DBG_ASSERT(condition)\
		if (condition) {\
			/* Passed test */\
		} else {\
			LOG("ASSERT FAILED! " << __FILE__ << "(" << __LINE__ << "): " << #condition);\
			exit(EXIT_FAILURE);\
		}

#else // !ANTS_DEBUG

	// Do nothing. Debugging is disabled:
	#define LOG(x)

	// Assertions disabled:
	#define DBG_ASSERT(condition)

#endif // ANTS_DEBUG

#endif // DEBUG_HPP
