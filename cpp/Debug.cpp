
// ================================================================================================
// -*- C++ -*-
// File: Debug.cpp
// Author: Guilherme R. Lampert
// Created on: 16/09/13
// Brief: Debug logger.
// ================================================================================================

#include "Debug.hpp"

DebugLog::DebugLog()
{
	Open("debug.txt");
}

DebugLog::~DebugLog()
{
	Close();
}

DebugLog & DebugLog::Instance()
{
	static DebugLog theDebugLog;
	return theDebugLog;
}

void DebugLog::Open(const std::string & filename)
{
	if (!file.is_open())
	{
		file.open(filename.c_str());
	}
}

void DebugLog::Close()
{
	if (file.is_open())
	{
		file.close();
	}
}
