
## AI bot for the "Ants AI Challenge"

Class assignment of an AI programming unit of my Games Development degree.

See: [aichallenge.org](http://ants.aichallenge.org/)

----

![sample run](https://bytebucket.org/glampert/ant-commander/raw/9760d4d36c377463fd59dd2a552a9858ee289623/sample.png "Sample run of the AI bot")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

