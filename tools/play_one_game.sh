#!/usr/bin/env sh

# maps/cell_maze/cell_maze_p02_06.map
# maps/example/tutorial1.map

./playgame.py --player_seed=42 --end_wait=0.25 --verbose --log_dir=game_logs --turns=500 --map_file=maps/random_walk/random_walk_02p_02.map "$@" \
	"python sample_bots/python/LeftyBot.py"  \
	"./AntCommander"
	"python sample_bots/python/HunterBot.py" \


#./playgame.py --player_seed=42 --end_wait=0.25 --verbose --log_dir=game_logs --turns=300 --map_file=maps/maze/maze_04p_01.map "$@" \
#	"python sample_bots/python/HunterBot.py" \
#	"python sample_bots/python/LeftyBot.py"  \
#	"python sample_bots/python/GreedyBot.py" \
#	"./AntCommander"

